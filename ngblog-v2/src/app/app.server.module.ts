import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { ServerModule } from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './main/app.component';

@NgModule({
  imports: [
    AppModule,
    ServerModule
],
  providers: [
    { provide: APP_BASE_HREF, useValue: 'http://localhost:4000/' },
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {}
